const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .copy('node_modules/jquery-steps/build/jquery.steps.min.js', 'public/js/jquery.steps.min.js')
    .copy('node_modules/parsleyjs/dist/parsley.min.js', 'public/js/parsley.min.js')
    .copy('node_modules/vue-form-wizard/dist/vue-form-wizard.js', 'public/js/vue-form-wizard.js')
    .copy('node_modules/vue-form-wizard/dist/vue-form-wizard.min.css', 'public/css/vue-form-wizard.css')
    .copy('node_modules/vee-validate/dist/vee-validate.js', 'public/js/vee-validate.js')
    .copy('node_modules/parsleyjs/dist/parsley.min.js', 'public/js/parsley.min.js')
    .copy('node_modules/vue-resource/dist/vue-resource.min.js', 'public/js/vue-resource.min.js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .version();
