<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://unpkg.com/vue-form-wizard/dist/vue-form-wizard.min.css">

    <script>
        window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};
    </script>
</head>
<body>
<div class="container">
    <h1>Stichting Studiebegeleiding Leiden</h1>

    <div id="root">
        @verbatim
        <form-wizard id="wizard" title="" subtitle="" @on-complete="onComplete">
            <tab-content title="Account" :before-change="validate('step-account')">
                <form data-vv-scope="step-account">
                    <div :class="{'form-group': true, 'has-danger': errors.has('step-account.userName')}">
                        <label for="userName">User name *</label>
                        <input id="userName" name="userName" type="text" class="form-control required" v-validate="'required'"
                               v-model="form.userName" :class="{'input': true, 'form-control': true, 'form-control-danger': errors.has('step-account.userName') }">
                        <span v-show="errors.has('step-account.userName')" class="help is-danger">@{{ errors.first('step-account.userName') }}</span>
                    </div>
                    <div :class="{'form-group': true, 'has-danger': errors.has('step-account.password')}">
                        <label for="password">Password *</label>
                        <input id="password" name="password" type="password" class="form-control required" v-validate="'required'"
                                v-model="form.password" :class="{'input': true, 'form-control': true, 'form-control-danger': errors.has('step-account.password') }">
                        <span v-show="errors.has('step-account.password')" class="help is-danger">@{{ errors.first('step-account.password') }}</span>
                    </div>
                    <div :class="{'form-group': true, 'has-danger': errors.has('step-account.confirm')}">
                        <label for="confirm">Confirm Password *</label>
                        <input id="confirm" name="confirm" type="password" class="form-control required" v-validate="'required|confirmed:password'"
                               :class="{'input': true, 'form-control': true, 'form-control-danger': errors.has('step-account.confirm') }">
                        <span v-show="errors.has('step-account.confirm')" class="help is-danger">@{{ errors.first('step-account.confirm') }}</span>
                    </div>
                    <p>(*) Mandatory</p>
                </form>
            </tab-content>
            <tab-content title="Profile">
                <form data-vv-scope="step-profile" :before-change="validate('step-profile')">
                    <div :class="{'form-group': true, 'has-danger': errors.has('name')}">
                        <label for="name">First name *</label>
                        <input id="name" name="name" type="text" class="form-control required" v-validate="'required'"
                               v-model="form.name" :class="{'input': true, 'form-control': true, 'form-control-danger': errors.has('name') }">
                        <span v-show="errors.has('name')" class="help is-danger">@{{ errors.first('name') }}</span>
                    </div>
                    <div class="form-group" id="app">
                        <label for="vueArea">Door Vue.js afgehandeld:</label>
                        <textarea id="vueArea" v-model="form.message" placeholder="add multiple lines"></textarea>
                    </div>
                    <p>(*) Mandatory</p>
                </form>
            </tab-content>
            <tab-content title="Finish">
                <form data-vv-scope="step-finish" :before-change="validate('step-finish')">
                    <div :class="{'form-check': true, 'has-danger': errors.has('acceptTerms')}">
                        <label class="form-check-label">
                            <input id="acceptTerms" name="acceptTerms" type="checkbox" class="required" v-validate="'required'"
                                   v-model="form.acceptTerms" class="form-check-input">
                            I agree with the Terms and Conditions.
                        </label>
                        <p v-show="errors.has('acceptTerms')" class="help is-danger">@{{ errors.first('acceptTerms') }}</p>
                    </div>
                </form>
            </tab-content>
        </form-wizard>
        @endverbatim
    </div>
</div>

<script src="{{ mix('/js/app.js') }}"></script>
<script src="{{ asset('/js/vue-form-wizard.js') }}"></script>
<script src="{{ asset('/js/vee-validate.js') }}"></script>
<script src="{{ asset('/js/vue-resource.min.js') }}"></script>
<script>

    Vue.use(VueFormWizard);
    Vue.use(VeeValidate);
    Vue.use(VueResource);

    window.vue = new Vue({
        el: '#root',
        http: {
            headers: {
                'X-CSRF-TOKEN': Laravel.csrfToken,
            }
        },
        data: {
            form: {
                userName: '',
                password: '',
                name: '',
                message: '',
                acceptTerms: false,
            }
        },
        methods: {
            validate: function(scope) {
                return function() {
                    var validator = this.$root.$validator;
                    return new Promise(function (resolve, reject) {
                        validator.validateAll(scope).then(function(result) {
                            if (result) {
                                resolve(true);
                            } else {
                                reject();
                            }
                        }).catch(function() {
                            reject();
                        });
                    });
                }
            },
            onComplete: function() {
                this.$http.post('', this.form).then(function(response) {
                    console.log(response);
                    document.querySelector('body').innerHTML = response.bodyText;
                });
            }
        }
    });
</script>
</body>
</html>