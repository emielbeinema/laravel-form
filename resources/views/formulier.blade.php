<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
</head>
<body>
<div class="container">
    <h1>Stichting Studiebegeleiding Leiden</h1>

    <form id="example-form" method="post">
        <h3>Account</h3>
        <section>
            <div class="form-group">
                <label for="userName">User name *</label>
                <input id="userName" name="userName" type="text" class="form-control required" required>
            </div>
            <div class="form-group">
                <label for="password">Password *</label>
                <input id="password" name="password" type="password" class="form-control required" required>
            </div>
            <div class="form-group">
                <label for="confirm">Confirm Password *</label>
                <input id="confirm" name="confirm" type="password" class="form-control required" required data-parsley-equalto="#password">
            </div>
            <p>(*) Mandatory</p>
        </section>
        <h3>Profile</h3>
        <section>
            <div class="form-group">
                <label for="name">First name *</label>
                <input id="name" name="name" type="text" class="form-control required" required>
            </div>
            <div class="form-group" id="app">
                <label for="vueArea">Door Vue.js afgehandeld:</label>
                <textarea id="vueArea" v-model="message" placeholder="add multiple lines"></textarea>
                <input name="vueInput" type="text" :value="message" style="display: none;">
            </div>
            <p>(*) Mandatory</p>
        </section>
        <h3>Finish</h3>
        <section>
            <input id="acceptTerms" name="acceptTerms" type="checkbox" class="required" required> <label for="acceptTerms">I agree with the Terms and Conditions.</label>
        </section>
        {{-- Pas op! Het CSRF-veld moeten we NIET lokaal opslaan. Met een oude token kunnen we immers het formulier niet submitten. --}}
        {{ csrf_field() }}
    </form>
</div>
<script src="{{ mix('/js/app.js') }}"></script>
<script src="{{ asset('/js/jquery.steps.min.js') }}"></script>
<script src="{{ asset('/js/parsley.min.js') }}"></script>

<script>
    var form = $("#example-form");
    var formData = {};

    var parsley = form.parsley();

    form.steps({
        headerTag: "h3",
        bodyTag: "section",
        onStepChanging: function (event, currentIndex, newIndex)
        {
            $(form.find('section')[currentIndex]).find('input').toArray().forEach(function(inputEl) {
                console.log(inputEl.name + ": " + inputEl.value);
                formData[inputEl.name] = inputEl.value;
            });

            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex)
            {
                return true;
            }
            // Forbid next action on "Warning" step if the user is to young
            if (newIndex === 3 && Number($("#age-2").val()) < 18)
            {
                return false;
            }
            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex)
            {
                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }
            return parsley.validate({group: 'block-' + currentIndex});
        },
        onFinishing: function (event, currentIndex)
        {
            return parsley.validate({group: 'block-' + currentIndex});
        },
        onFinished: function (event, currentIndex)
        {
            console.log(formData);
            form.submit();
        }
    });

    /*
     * We delen de inputs in het formulier voor parsley in in groepen die corresponderen met de stappen. Zo
     * kunnen we altijd alleen de huidige pagina checken voor we naar de volgende stap gaan.
     */
    $('section').each(function(index, section) {
        $(section).find(':input').attr('data-parsley-group', 'block-' + index);
    });

    new Vue({
        el: '#app',
        data: {
            message: ''
        }
    });
</script>
</body>
</html>